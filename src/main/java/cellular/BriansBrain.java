package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;


public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    /**
     *
     * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
     * provided size
     *
     * @param height The height of the grid of cells
     * @param width  The width of the grid of cells
     */
    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }
    }

    @Override
    public int numberOfRows() {
        // TODO
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        // TODO
        return currentGeneration.numColumns();
    }

    @Override
    public CellState getCellState(int row, int col) {
        // TODO

        return currentGeneration.get(row, col); //return the state of the cell in the given row and column
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        // TODO

        for(int row=0; row<numberOfRows();row++) {
            for(int col=0 ; col <numberOfColumns(); col++) {
                CellState state = getNextCell(row,col);
                nextGeneration.set(row, col, state);
            }
        }

        currentGeneration = nextGeneration;

    }

    @Override
    public CellState getNextCell(int row, int col) {
        // TODO
        boolean isAlive = getCellState(row, col) == CellState.ALIVE;
        boolean isDead = getCellState(row, col) == CellState.DEAD;
        boolean isDying = getCellState(row, col) == CellState.DYING;
        int livingNeighbours = countNeighbors(row,col,CellState.ALIVE);

        if(isAlive)
            return CellState.DYING;
        if(isDying)
            return CellState.DEAD;
        if(isDead && (livingNeighbours==2))
            return CellState.ALIVE;

        return getCellState(row, col);

    }

    /**
     * Calculates the number of neighbors having a given CellState of a cell on
     * position (row, col) on the board
     *
     * Note that a cell has 8 neighbors in total, of which any number between 0 and
     * 8 can be the given CellState. The exception are cells along the boarders of
     * the board: these cells have anywhere between 3 neighbors (in the case of a
     * corner-cell) and 5 neighbors in total.
     *
     * @param x     the x-position of the cell
     * @param y     the y-position of the cell
     * @param state the Cellstate we want to count occurences of.
     * @return the number of neighbors with given state
     */
    private int countNeighbors(int row, int col, CellState state) {
        // TODO
        int numNeighbours = 0;

        for(int dx=-1; dx<=1; dx++) {
            for(int dy=-1; dy<=1; dy++) {
                if(dx==0 && dy==0) {
                    continue;
                }
                if(isValidLocation(row+dx,col+dy)) {
                    CellState neighbour = getCellState(row+dx, col+dy);
                    if(neighbour==state) {
                        numNeighbours++;
                    }
                }
            }
        }

        return numNeighbours;

    }

    @Override
    public IGrid getGrid() {

        return currentGeneration;
    }



    private boolean isValidLocation(int row, int col) {
        if(row<0 || row>=numberOfRows())
            return false;
        if(col<0 || col>=numberOfColumns())
            return false;
        return true;
    }
}
